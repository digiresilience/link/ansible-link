<!-- markdownlint-disable -->
## Role Variables

| Variable     | Default Value  | Description  |
| ------------ | -------------- | ------------ |
| database_authenticator_username | `app_graphile_auth` | The PostgreSQL user for the authenticator user for Metamigo. |
| database_visitor_username | `app_user` | The PostgreSQL user for the visitor user for Metamigo. |
| label_studio_db_host | ~ | The label studio PostgreSQL host. |
| label_studio_db_name | `label_studio` | the postgres database label studio will use |
| label_studio_db_password | ~ | The label studio PostgreSQL password. |
| label_studio_db_port | `5432` | the label studio postgres port |
| label_studio_db_username | `label_studio` | the postgres user label studio will use |
| label_studio_docker_image | `heartexlabs/label-studio:v1.4.1` | the label studio docker hub image |
| label_studio_enabled | `false` | whether or not to install label studio on the docker host |
| label_studio_frontend_domain | ~ | The label studio frontend domain. |
| leafcutter_contributor_id | ~ | The Leafcutter contributor ID. |
| leafcutter_contributor_name | ~ | The Leafcutter contributor name. |
| leafcutter_enabled | `false` | Whether to enable Leafcutter. |
| leafcutter_label_studio_api_key | ~ | The label studio API key. |
| leafcutter_label_studio_api_url | ~ | The label studio API URL. |
| leafcutter_opensearch_api_url | ~ | The Opensearch API URL. |
| leafcutter_opensearch_password | ~ | The Opensearch password. |
| leafcutter_opensearch_username | ~ | The Opensearch username. |
| leafcutter_zammad_api_key | ~ | The Zammad api key. |
| leafcutter_zammad_api_url | ~ | The Zammad API URL. |
| link_stack_enabled | `false` | Whether to deploy the new and improved Link Shell. |
| metamigo_api_domain | `metamigo-api` | The API domain for Metamigo. This can be the internal Docker DNS name and will not be accessed externally. |
| metamigo_db_host | `metamigo-postgresql` | The hostname for Metamigo's PostgreSQL. |
| metamigo_db_name | `metamigo` | The PostgreSQL database for Metamigo. |
| metamigo_db_username | `metamigo` | The PostgreSQL user for Metamigo. |
| metamigo_frontend_domain | `"{{ (zammad_domain | split('.', 1))[0] + '-admin.' + (zammad_domain | split('.', 1))[1] }}"` | The frontend domain for Metamigo. |
| metamigo_root_db_username | `postgres` | The root PostgreSQL user for Metamigo. |
| zammad_db_create | `false` | Whether or not the PostgreSQL database should be created by zammad on first boot. |
| zammad_db_name | `zammad_production` | The PostgreSQL database for Zammad. |
| zammad_db_password | **REQUIRED** | The PostgreSQL password for Zammad. |
| zammad_db_username | `postgres` | The PostgreSQL user for Zammad. |
| zammad_docker_restart_policy | `unless-stopped` | The restart policy to use in docker-compose. |
| zammad_domain | **REQUIRED** | The domain where your zammad instance will be available. |
| zammad_es_enabled | `false` | Whether to use a remote Elasticsearch. If false, a local Elasticsearch container will be created in the docker-compose stack. |
| zammad_es_host | ~ | The hostname of the Elasticsearch instance. |
| zammad_es_namespace | `zammad` | The namespace to use in Elasticsearch. |
| zammad_es_port | ~ | The port of the Elasticsearch instance |
| zammad_es_reindex | `true` | Whether or not to reindex when starting. |
| zammad_es_schema | `http` | The protocol scheme to use when connecting. WARNING: INSECURE BY DEFAULT. |
| zammad_es_ssl_verify | `true` | Whether to verify the remote Elasticsearch TLS certificate. |
| zammad_exporter_listen_address | ~ | The host address the exporter should listen on (should be an internal interface) required when monitoring is enabled. |
| zammad_image_cadvisor | `gcr.io/cadvisor/cadvisor:v0.47.0` | The cAdvisor Docker image to use. |
| zammad_image_elasticsearch | `registry.gitlab.com/digiresilience/link/docker-elasticsearch:master` | The Elasticsearch Docker image to use. |
| zammad_image_graphql | `registry.gitlab.com/digiresilience/link/zammad-graphql:master` | The Zammad GraphQL Docker image to use. |
| zammad_image_memcached | `memcached:1.6.18-alpine` | The memcached Docker image to use. |
| zammad_image_metamigo | `registry.gitlab.com/digiresilience/link/metamigo:develop` | The Metamigo Docker image to use. |
| zammad_image_metamigo_postgresql | `postgres:12` | The PostgreSQL Docker image to use for Metamigo's PostgreSQL server. |
| zammad_image_postgresql | `postgres:9.6` | The PostgreSQL Docker image to use for Zammad's PostgreSQL server. |
| zammad_image_postgresql_exporter | `wrouesnel/postgres_exporter` | The postgres_exporter Docker image to use. |
| zammad_image_protonmail | `ana0/protonmail-bridge:latest` | The protonmail-bridge Docker image to use. |
| zammad_image_redis | `redis:7-bullseye` | The redis Docker image to use. |
| zammad_image_signald | `signald/signald:0.23.2` | The signald Docker image to use. |
| zammad_image_zammad | `registry.gitlab.com/digiresilience/link/docker-zammad:main` | The Zammad Docker image to use, which should include all the CDR Link customisations. |
| zammad_image_zammad_exporter | `registry.gitlab.com/guardianproject-ops/zammad_exporter:master` | The zammad_exporter Docker image to use. |
| zammad_letsencrypt_email | ~ | The email address used to register the LE certs, used for account recovery and renewal warnings. (Required when not using CloudFlare access.) |
| zammad_memcached_size | `256M` | The default memcached buffer size. |
| zammad_monitoring_token | ~ | The monitoring token used to fetch zammad metrics. This will be set on zammad during setup and used by the zammad_exporter. required when monitoring is enabled. |
| zammad_protonmail_enabled | `false` | whether to enable protonmail bridge or not |
| zammad_standalone | `false` | whether or not to install postgresql and elasticsearch on the docker host |

<!-- markdownlint-enable -->
