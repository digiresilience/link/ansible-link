#!/usr/bin/env python3
"""
This script attempts to set the monitoring token on a Zammad instance
running on the host in a docker compose configuration.

It is idempotent and designed to be used with Ansible. On a succesful run, the
string "new token set" will be output to stdout if the token was changed. If
the token was not changed, nothing will be printed. When an error ocurrs, the
rc will be > 0 and diagnostic information will be printed to stderr.

It uses the Zammad rails console (runner) to set the token.
ref: https://docs.zammad.org/en/latest/admin-console.html

It queries the posqtgresql database directly to fetch the current token. This
is done because querying the database is much faster than using the rails
console.

usage: set-monitoring-token.py YOUR_NEW_TOKEN


Author:
    Abel Luck <abel@guardianproject.info>

License:
    (C) 2019
    GNU Affero General Public License (AGPL) v3+
    https://www.gnu.org/licenses/agpl-3.0.en.html
"""

from __future__ import print_function
import re
import sys
import subprocess


def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)


if len(sys.argv) != 2:
    print("usage: {} <NEW TOKEN>".format(sys.argv[0]))
    sys.exit(1)

token = sys.argv[1]
if token == None or len(token.strip()) == 0:
    eprint("Token is empty. It must be passed.")
    sys.exit(1)


def get_current_token():
    cmd = [
        "docker",
        "exec",
        "zammad_zammad-railsserver_1",
        "rails",
        "runner",
        "p Setting.get('monitoring_token')",
    ]
    try:
        result = subprocess.run(
            cmd,
            check=True,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            universal_newlines=True,
        )
        match = re.search(r"^\"(.*)\"$", result.stdout, re.MULTILINE)
        if match is None or len(match.groups()) != 1:
            eprint("Token could not be parsed")
            sys.exit(3)
        return match.group(1).strip()
    except subprocess.CalledProcessError as e:
        eprint("Failed to get current monitoring token with rc: %d" % (e.returncode))
        eprint(" ".join(cmd))
        if len(e.stdout) > 0:
            eprint("STDOUT")
            eprint(e.stdout)
        if e.stderr is not None:
            eprint("STDERR")
            eprint(e.stderr)
        sys.exit(2)


def set_token(token):
    cmd = [
        "docker",
        "exec",
        "zammad_zammad-railsserver_1",
        "rails",
        "runner",
        "Setting.set('monitoring_token', '{}')".format(token),
    ]
    try:
        subprocess.run(cmd, check=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    except subprocess.CalledProcessError as e:
        eprint("Failed to set monitoring token with rc: %d" % (e.returncode))
        eprint(" ".join(cmd).replace(token, "XXXX"))
        if len(e.stdout) > 0:
            eprint("STDOUT")
            eprint(e.stdout.decode("utf-8"))
        if e.stderr is not None:
            eprint("STDERR")
            eprint(e.stderr.decode("utf-8"))
        sys.exit(4)


new_token = token.strip()
old_token = get_current_token()

if old_token != new_token:
    set_token(new_token)
    print("new token set")
