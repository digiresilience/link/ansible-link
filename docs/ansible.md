<!-- markdownlint-disable -->
## Roles

| Role Name    | Description  |
| ------------ | ------------ |
| [digiresilience.link.docker_link](./roles/docker_link) | CDR Link docker-compose stack |

## Playbooks

| Playbook Name    | Description  |
| ---------------- | ------------ |
| [digiresilience.link.db_setup](./playbooks/digiresilience.link.db_setup.yml) | Configure PostgreSQL with users, databases and permissions |
| [digiresilience.link.infrastructure](./playbooks/digiresilience.link.infrastructure.yml) | Deploy infrastructure resources with Terraform to AWS and Cloudflare |
| [digiresilience.link.system_baseline](./playbooks/digiresilience.link.system_baseline.yml) | Prepare system for CDR Link deployment |
| [digiresilience.link.zammad](./playbooks/digiresilience.link.zammad.yml) | *TODO* |

<!-- markdownlint-enable -->
