---
- name: Deploy infrastructure resources with Terraform to AWS and Cloudflare
  hosts: aws
  gather_facts: false
  tags: infrastructure
  environment:
    AWS_PROFILE: "{{ aws_profile }}"
    AWS_REGION: "{{ aws_region }}"
    CLOUDFLARE_API_TOKEN: "{{ cloudflare_token }}"
  roles:
    - role: sr2c.terraform.terraform_s3_backend
      vars:
        terraform_state_s3_bucket_name: "{{ aws_profile }}-terraform-state"
        terraform_state_dynamodb_table_name: "{{ aws_profile }}-terraform-lock"
      delegate_to: localhost

    - role: sr2c.terraform.terraform_module
      vars:
        terraform_module_backend_config:
          s3: "{{ terraform_s3_backend_config | combine({'key': 'aws_base'}) }}"
        terraform_module_source: "{{ playbook_dir }}/../../terraform-modules/terraform-aws-cdr-link"
        terraform_module_inputs:
          namespace: "{{ namespace }}"
          name: link
          tenant: "{{ tenant }}"
          stage: "{{ stage }}"
        terraform_module_outputs_var: "terraform_outputs_aws"
      delegate_to: localhost

    - role: sr2c.terraform.terraform_module
      vars:
        terraform_module_backend_config:
          s3: "{{ terraform_s3_backend_config | combine({'key': 'cloudflare'}) }}"
        terraform_module_source: "{{ playbook_dir }}/../../terraform-modules/terraform-cloudflare-cdr-link"
        terraform_module_inputs:
          namespace: "{{ namespace }}"
          name: link
          tenant: "{{ tenant }}"
          stage: "{{ stage }}"
          cloudflare_account_id: "{{ cloudflare_account_id }}"
          cloudflare_zone_id: "{{ cloudflare_zone_id }}"
          client_admin_emails: "{{ client_admin_emails }}"
          client_agent_emails: "{{ client_agent_emails }}"
          super_admins_group_ids: "{{ super_admins_group_ids }}"
        terraform_module_outputs_var: "terraform_outputs_cloudflare"
      delegate_to: localhost

  tasks:
    - name: Save Terraform outputs for later
      community.sops.sops_encrypt:
        path: "{{ inventory_dir }}/host_vars/{{ inventory_hostname }}.generated.sops.yml"
        content_yaml:
          cloudflare_tunnel_name: "{{ terraform_outputs_cloudflare.tunnel_name }}"
          cloudflare_tunnel_id: "{{ terraform_outputs_cloudflare.tunnel_id }}"
          cloudflare_tunnel_secret: "{{ terraform_outputs_cloudflare.tunnel_secret }}"
          metamigo_cloudflare_access_audience: "{{ terraform_outputs_cloudflare.metamigo_audience }}"
          metamigo_domain: "{{ terraform_outputs_cloudflare.metamigo_domain }}"
          zammad_domain: "{{ terraform_outputs_cloudflare.zammad_domain }}"
          instance_id: "{{ terraform_outputs_aws.instance_id }}"
          metamigo_db_password: "{{ terraform_outputs_aws.rds_metamigo_password }}"
          database_authenticator_password: "{{ terraform_outputs_aws.rds_metamigo_authenticator_password }}"
          metamigo_db_name: "metamigo"
          zammad_db_password: "{{ terraform_outputs_aws.rds_zammad_password }}"
          rds_hostname: "{{ terraform_outputs_aws.rds_hostname }}"
          rds_superuser_user: "{{ terraform_outputs_aws.rds_superuser_user }}"
          rds_superuser_password: "{{ terraform_outputs_aws.rds_superuser_password }}"
          zammad_monitoring_token: "{{ terraform_outputs_aws.zammad_monitoring_token }}"
          elasticsearch_host: "{{ terraform_outputs_aws.es_hostname }}"
          cloudflare_access_monitoring_client_id: "{{ terraform_outputs_cloudflare.access_client_id }}"
          cloudflare_access_monitoring_client_secret: "{{ terraform_outputs_cloudflare.access_client_secret }}"
      delegate_to: localhost
